----------------------------------------------------------------------------------------------------
-- SeasonsCutter
----------------------------------------------------------------------------------------------------
-- Purpose:  Update AI of cutters
--
-- Copyright (c) Realismus Modding, 2019
----------------------------------------------------------------------------------------------------

SeasonsCutter = {}

function SeasonsCutter.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(Cutter, specializations)
end

function SeasonsCutter.registerOverwrittenFunctions(vehicleType)
    SpecializationUtil.registerOverwrittenFunction(vehicleType, "setAIFruitRequirements", SeasonsCutter.setAIFruitRequirements)
end

---Allow partial cutting into withered and germination failed spots to be more lenient on workers in regular fields.
--This has to be a new spec because we add a new override to the Cutter spec
function SeasonsCutter:setAIFruitRequirements(superFunc, fruitType, minGrowthState, maxGrowthState)
    superFunc(self, fruitType, minGrowthState, maxGrowthState)

    local fruitTypeIndex = self.spec_cutter.currentInputFruitTypeAI
    local fruitType = g_fruitTypeManager:getFruitTypeByIndex(fruitType)

    -- Germ failed
    self:addAIFruitRequirement(fruitTypeIndex, SeasonsGrowth.GERMINATION_FAILED_STATE - 1, SeasonsGrowth.GERMINATION_FAILED_STATE - 1)

    -- Withered
    if fruitType.witheringNumGrowthStates > fruitType.numGrowthStates then
        self:addAIFruitRequirement(fruitTypeIndex, fruitType.maxHarvestingGrowthState + 1, fruitType.maxHarvestingGrowthState + 1)
    end
end
